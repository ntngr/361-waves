
// NTA - 361-basette
nomeFile = "00123";
// X size: 644.997
// Y size: 404.965
Zdim = 1804.85400390625;
fattoreScala = 0.05;
ZdimSc = Zdim*fattoreScala; echo (ZdimSc);
Xpos = -322.17;
Ypos = -202;
Zpos = -902;



color("gray")
union (){
    translate([ 0.0 , 0.0 , ZdimSc/2 ])
    ritrattoPOS();
    basetta3D();
    }

module ritrattoPOS(){
translate([ 0.0 , 0.0 , 4-2 ])
ritrattoScalato();
}

module ritrattoScalato(){
scale([fattoreScala,fattoreScala,fattoreScala])
importatoCentrato();
}

module importatoCentrato(){
translate([ Xpos , Ypos , Zpos ])
 import ("00123-Test.stl");
}

module basetta3D(){
color("yellow")
difference(){
    basetta();
    scritta3D();
}
}


module basetta() {
color("red")
cylinder(r=18, h=4, $fn=6);
}

module scritta3D() {
    color("blue")
    mirror([0,1,0])
    translate([0,0,0.5])
    text(nomeFile,size=6,halign="center", valign="center");
}


    