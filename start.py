#!/usr/bin/python
from google.cloud import speech
from pydub import AudioSegment
import argparse, wave, array, math, glob, os, pyaudio, shutil, io
import numpy as np
#----------------------------------------------

#---------REGISTRAZIONE--AUDIO-----------------

parser = argparse.ArgumentParser(description='AudioMic->WAVE->OpenSCAD')
parser.add_argument('-t', action='store', dest='secondi', type=int, help='Inserisci i Secondi di registrazione', default=3)
# parser.add_argument('input_filename', action='store', help='Input WAVE filename', default="./test/test.wav")
# parser.add_argument('output_filename', action='store', help='Output OpenSCAD filename', default="test/test.scad")
#parser.add_argument('-t', action='store', dest='sample_time', type=float, help='Time length of average for each segment (ms)', default=10.0)
parser.add_argument('-hmin', action='store', dest='altezza_min', type=float, help='Min Altezza Fetta Remappata', default=8.0)
parser.add_argument('-hmax', action='store', dest='altezza_max', type=float, help='Max Altezza Fetta Remappata', default=16.0)
parser.add_argument('-rhole', action='store', dest='circle_hole_radius', type=float, help='Raggio del Foro', default=2.0)
parser.add_argument('-dext', action='store', dest='circle_diametro', type=float, help='Diametro Esterno', default=1.0)
parser.add_argument('-v', '--version', action='version', version='%(prog)s 1.1')
results = parser.parse_args()

os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="./lib/freespeech-374309-5a6c7dd7c7da.json"

# Record in chunks of 1024 samples
chunk = 1024

# 16 bits per sample
sample_format = pyaudio.paInt16
chanels = 2

# Record at 44400 samples per second
smpl_rt = 44400
seconds = results.secondi
filename = "./test/test.wav"

# Create an interface to PortAudio
pa = pyaudio.PyAudio()

stream = pa.open(format=sample_format, channels=chanels,
                 rate=smpl_rt, input=True,
                 frames_per_buffer=chunk)

# os.system('clear')
print('Inizio Registrazione...')
print('Secondi:')
print(seconds)
# Initialize array that be used for storing frames
frames = []

# Store data in chunks for X seconds
for i in range(0, int(smpl_rt / chunk * seconds)):
    data = stream.read(chunk)
    frames.append(data)

# Stop and close the stream
stream.stop_stream()
stream.close()

# Terminate - PortAudio interface
pa.terminate()

print('FATTO !!! ')

# Save the recorded data in a .wav format
sf = wave.open(filename, 'wb')
sf.setnchannels(chanels)
sf.setsampwidth(pa.get_sample_size(sample_format))
sf.setframerate(smpl_rt)
sf.writeframes(b''.join(frames))
sf.close()


# Creates google client
client = speech.SpeechClient()

# Full path of the audio file, Replace with your file name
file_name = os.path.join(os.path.dirname(__file__),"./test/test.wav")

#Loads the audio file into memory
with io.open(file_name, "rb") as audio_file:
    content = audio_file.read()
    audio = speech.RecognitionAudio(content=content)

config = speech.RecognitionConfig(
    encoding=speech.RecognitionConfig.AudioEncoding.LINEAR16,
    audio_channel_count=2,
    language_code="it-IT",
)

# Sends the request to google to transcribe the audio
response = client.recognize(request={"config": config, "audio": audio})
scritta=""
# Reads the response
for result in response.results:
    print("Transcript: {}".format(result.alternatives[0].transcript))
    #sound = AudioSegment.from_wav('test.wav')
    #sound.export('test.mp3', format='mp3')
    shutil.copy('test/test.wav', 'test/'+format(result.alternatives[0].transcript)+'.wav')
    #os.rename('test.wav', format(result.alternatives[0].transcript)+'.wav')
    #os.rename('test.mp3', format(result.alternatives[0].transcript)+'.mp3')
#---------CONVERSIONE-3D-OPENSCAD--------------
#----------------------------------------------
    scritta = format(result.alternatives[0].transcript)
#----------------------------------------------
def main():
    # #Raccolta argomenti
    # parser = argparse.ArgumentParser(description='WAVE->OpenSCAD')
    # parser.add_argument('input_filename', action='store', help='Input WAVE filename', default="./test/test.wav")
    # parser.add_argument('output_filename', action='store', help='Output OpenSCAD filename', default="test/test.scad")
    # parser.add_argument('-t', action='store', dest='sample_time', type=float, help='Time length of average for each segment (ms)', default=10.0)
    # parser.add_argument('-hmin', action='store', dest='altezza_min', type=float, help='Min Altezza Fetta Remappata', default=8.0)
    # parser.add_argument('-hmax', action='store', dest='altezza_max', type=float, help='Max Altezza Fetta Remappata', default=16.0)
    # parser.add_argument('-rhole', action='store', dest='circle_hole_radius', type=float, help='Raggio del Foro', default=2.0)
    # parser.add_argument('-dext', action='store', dest='circle_diametro', type=float, help='Diametro Esterno', default=1.0)
    # parser.add_argument('-v', '--version', action='version', version='%(prog)s 1.1')
    # results = parser.parse_args()

    #Output welcome message
    print ("Welcome to 361-WAVES->OpenSCAD")
    print ("------------------------------")

    #Open input file
    try:
        #Attempt to open file
        input_file = wave.open("test/test.wav", 'r') #results.input_filename

    except (IOError, wave.Error) as e:
        #Oh dear
        print ("An error occurred opening input file :"), str(e)

    # Analisi del file .wav
    #Print message
    print("-------------INPUT----------------")
    print("Info file AUDIO aperto:")
    print("\tNumero di Canali:\t%i" % input_file.getnchannels())
    print("\tSample Rate:\t\t%iHz" % input_file.getframerate())
    print("\tDurata:\t\t\t%.2fs" % (float(input_file.getnframes()) / input_file.getframerate()))
    print("\tTesto riconosciuto:\t"+scritta)
    print("")
        #Check number of channels
    if input_file.getnchannels() == 2:
        #Only expecting one channel
        print("\tTrovati due Canali")
        print("\tAnalizzo Canale SX")
        print("")
    #Check sample width
    if input_file.getsampwidth() != 2:
        #Only expecting two bytes (16 bits) per sample
        sys.exit("Problema: Campione con un solo Canale, atteso 16-bit")
    #Calculate number of audio frames per average
    avg_frame_count = int((results.secondi / 300) * input_file.getframerate())

    #Create waves
    waves = []
    avgs = []
    while 1:
        # Legge tutti i frame del file
        str_frames = input_file.readframes(avg_frame_count)
        #Check frames were read
        if not str_frames:
            break;
        #Convert frames to array of shorts
        frames = array.array('h', str_frames)
        if input_file.getnchannels() == 2:
            frames = frames[::2]
        frames_abs = [abs(x) for x in frames]
        avg = np.mean(frames_abs)
        waves.append(Wave)
        avgs.append(avg)

    avgsScaled = scaleAvgs(avgs, results.altezza_min, results.altezza_max)

    #Print message
    print("-------------------OUTPUT-------------------")
    print("Info file 3D in uscita:")
    print("\tNumero di Punti:\t%i" % len(waves))
    print("\tDiametro Esterno:\t%.2fmm" % results.circle_diametro)
    print("\tDiametro Interno:\t%.2fmm" % (results.circle_hole_radius*2))
    print("--------------------------------------------")
    # print(frames)
    #Open output file
    scrittaFileOut = ""
    for i in range(len(scritta)):
        if scritta[i] == ' ':
            scrittaFileOut = scrittaFileOut + '_'
        else:
            scrittaFileOut = scrittaFileOut + scritta[i]
    try:
        #Attempt to open file
        output_file = open(scrittaFileOut + '.scad', 'w')
    except IOError as e:
        #Oh dear
        print("An error occurred opening output file :", str(e))

    # Calcolo rapporto
    rapporto=float(360.0/float(len(avgsScaled)))
    #print(rapporto)
    listaRotate=[]
    j=0
    for i in range(0,len(avgsScaled)):
        listaRotate.append(j)
        #print(j)
        j+=rapporto


    #Prepare and output template
    templateCustom = prepare_template_custom(avgsScaled,results.circle_hole_radius,listaRotate,results.circle_diametro,scritta)
    output_file.write(templateCustom)

    #Close output file
    output_file.close()

#----------------------------------------------
#Scale method Radius
def scaleAvgs(avgs, new_min, new_max):
    #Retrieve values
    vals = avgs

    #Compute scale factor
    old_min = min(vals)
    old_max = max(vals)
    old_range = float(old_max - old_min)
    new_range = float(new_max - new_min)
    if old_range == 0:
        factor = 1
    else:
        factor = new_range / old_range
        print(factor)

    avgsScaled=[]
    #Scale each element
    for x in avgs:
        avgsScaled.append(((x - old_min) * factor) + new_min)

    return avgsScaled

#----------------------------------------------
#----------------------------------------------
#Wave template
class Wave:
    def __init__(radius):
        self.radius = radius

#----------------------------------------------
def prepare_template_custom(avgs,circle_hole_radius,listaRotate,circle_diametro,scritta):

    return """

    //Dimensions
    altezze = {};
    lenAltezze = {};
    hole_radius = {};
    listaRotate={};


    diametroEsterno={};
    scritta="{}";
    echo(len(scritta));
    spaziatura=len(scritta)*7.8;
    echo (spaziatura);
    posizioneVertScritta = max(altezze);
    echo (posizioneVertScritta);

    $fn=30;

        module fettina(a, r, h){{
        // a:angle, r:radius, h:height
        rotate_extrude(angle=a) square([r,h]);
        }}

difference(){{
        difference(){{

                union() {{
                    for (a = [ 0 : lenAltezze - 1 ])
                            rotate(listaRotate[a],[0,0,1]) {{
                                fettina(listaRotate[1],diametroEsterno/2,altezze[a]);
                    }}
                }}

                cylinder(diametroEsterno*4, r=hole_radius, center=true);
        }}

        circletext(scritta,textsize=2,degrees=spaziatura,top=false, thickness=1);
    }}


module
circletext(scritta,textsize=20,myfont="Arial",radius=(diametroEsterno/2),thickness=1,degrees=360,top=true){{

        chars=len(scritta)+1;

        for (i = [1:chars]) {{

rotate([0,0,(top?1:-1)*(degrees/2-i*(degrees/chars))])

translate([0,(top?1:-1)*radius+1,(posizioneVertScritta/2)-textsize])
                                                rotate ([90,0,0])
                        linear_extrude(thickness)
                                                text(scritta[i-1],halign="center",font=myfont,size=textsize);
        }}
}}




    """ .format(avgs,len(avgs),circle_hole_radius,listaRotate,circle_diametro,scritta)

#----------------------------------------------

#Entry point
if __name__ == "__main__":
    main()

#----------------------------------------------
