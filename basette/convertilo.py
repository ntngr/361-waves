import math
import stl
from stl import mesh
import numpy

import os
import sys

if len(sys.argv) < 2:
    sys.exit('Usage: %s [stl file]' % sys.argv[0])

if not os.path.exists(sys.argv[1]):
    sys.exit('ERRORE: file %s non trovato!' % sys.argv[1])

def find_mins_maxs(obj):
    minx = maxx = miny = maxy = minz = maxz = None
    for p in obj.points:

        if minx is None:
            minx = p[stl.Dimension.X]
            maxx = p[stl.Dimension.X]
            miny = p[stl.Dimension.Y]
            maxy = p[stl.Dimension.Y]
            minz = p[stl.Dimension.Z]
            maxz = p[stl.Dimension.Z]
        else:
            maxx = max(p[stl.Dimension.X], maxx)
            minx = min(p[stl.Dimension.X], minx)
            maxy = max(p[stl.Dimension.Y], maxy)
            miny = min(p[stl.Dimension.Y], miny)
            maxz = max(p[stl.Dimension.Z], maxz)
            minz = min(p[stl.Dimension.Z], minz)
    return minx, maxx, miny, maxy, minz, maxz

main_body = mesh.Mesh.from_file(sys.argv[1])

minx, maxx, miny, maxy, minz, maxz = find_mins_maxs(main_body)

minx=round(minx,3)
maxx=round(maxx,3)
miny=round(miny,3)
maxy=round(maxy,3)
minz=round(minz,3)
maxz=round(maxz,3)

xsize = round(maxx-minx,3)
ysize = round(maxy-miny,3)
zsize = round(maxz-minz,3)

midx = round(xsize/2,3)
midy = round(ysize/2,3)
midz = round(zsize/2,3)

ctrx = round(-minx+(-midx),4)
ctry = round(-miny+(-midy),4)
ctrz = round(-minz+(-midz),4)


print("")
print ("// File:", sys.argv[1])
lst = ['obj =("',sys.argv[1],'");']
obj = ['\t\timport("',sys.argv[1],'");']

print ("// X size:",xsize)
print ("// Y size:", ysize)
print ("// Z size:", zsize)
print ("// X position:",minx)
print ("// Y position:",miny)
print ("// Z position:",minz)

#--------------------

print("")
print("Positions: NE, NW, SW, SE, Centre XY, Centre All")

print("")
print ("// NE")
print("translate([",-minx,",",-miny,",",-minz,"])")
print (' import ("',sys.argv[1],'");',sep='')

print("")
print ("// NW")
print("translate([",-minx + (-xsize),",",-miny,",",-minz,"])")
print (' import ("',sys.argv[1],'");',sep='')

print("")
print ("// SW")
print("translate([",-minx + (-xsize),",",-miny +(-ysize),",",-minz,"])")
print (' import ("',sys.argv[1],'");',sep='')

print("")
print ("// SE")
print("translate([",-minx,",",-miny+(-ysize),",",-minz,",","])")
print (' import ("',sys.argv[1],'");',sep='')

print ("")
ctrx = round(-minx+(-midx),4)
ctry = round(-miny+(-midy))
ctrz = round(-minz+(-midz))

print ("// Center XY")
print("translate([",ctrx, ",",ctry,",",-minz,"])")
print (' import ("',sys.argv[1],'");',sep='')

print ("")
print ("// Centre All")
print("translate([",ctrx, ",",ctry,",",ctrz,"])")
print (' import ("',sys.argv[1],'");',sep='')
print ("")

nomeFile = (sys.argv[1])[0:5]
nomeOrig = (sys.argv[1])
outFile = nomeFile+".scad"
print (outFile)
#----------------------------------------------
def prepare_template_custom(nomeFile,zsize,ctrx,ctry,ctrz,nomeOrig):

    return """
// NTA - 361-basette
nomeFile = "{}";

// X size: 644.997
// Y size: 404.965
Zdim = {};
fattoreScala = 50;
ZdimSc = Zdim*fattoreScala; echo (ZdimSc);
Xpos = {};
Ypos = {};
Zpos = {};
nomeOrig = "{}";


color("gray")
union (){{
    translate([ 0.0 , 0.0 , ZdimSc/2 ])
    ritrattoPOS();
    basetta3D();
    }}

module ritrattoPOS(){{
translate([ 0.0 , 0.0 , 13 ])
ritrattoScalato();
}}

module ritrattoScalato(){{
scale([fattoreScala,fattoreScala,fattoreScala])
importatoCentrato();
}}

module importatoCentrato(){{
translate([ Xpos , Ypos , Zpos ])
 import(nomeOrig);
}}

module basetta3D(){{
color("yellow")
difference(){{
    basetta();
    scritta3D();
}}
}}


module basetta() {{
color("red")
cylinder(r=18, h=4, $fn=6);
}}

module scritta3D() {{
    color("blue")
    mirror([0,1,0])
    translate([0,0,0.5])
    text(nomeFile,size=6,halign="center", valign="center");
}}


    """ .format(nomeFile,zsize,ctrx,ctry,ctrz,nomeOrig)

#----------------------------------------------


try:
    output_file = open(outFile, 'w')
except IOError as e:
    print("Errore nell'apertura del file .SCAD :", str(e))

templateCustom = prepare_template_custom(nomeFile,zsize,ctrx,ctry,ctrz,nomeOrig)
output_file.write(templateCustom)

output_file.close()
